import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SocialNetworkProvider } from '../../providers/social-network/social-network';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public data: object = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public SN: SocialNetworkProvider) {
  }

  facebooksignup(){
    this.SN.facebookdata().then((resp) => {
      console.log('facebook',resp);
    });
  }

  googlesignup(){
    this.SN.googledata().then((resp) => {
      console.log('google',resp);
    });
  }

  linkedinsignup(){
    this.SN.LinkedIndata().then((resp) => {
      console.log('LinkedIndata',resp);
    });
  }
  twittersignup(){
    this.SN.Twitterdata().then((resp) => {
      console.log('twitterdata',resp);
    });
  }
  registersubmit(form: NgForm){
    console.log(form);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
