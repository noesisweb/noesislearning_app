import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SocialNetworkProvider } from '../../providers/social-network/social-network';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public SN: SocialNetworkProvider) {
  }

  facebookdata(){
    this.SN.facebookdata().then((resp) => {
      console.log(resp);
    });
  }

}
