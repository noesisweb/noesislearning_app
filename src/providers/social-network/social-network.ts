import { Injectable } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { LinkedIn } from '@ionic-native/linkedin';
import { TwitterConnect } from '@ionic-native/twitter-connect';


@Injectable()
export class SocialNetworkProvider {
  public params = new Array<string>();
  public scopes = new Array<string>();
  constructor(public fb:Facebook, public googlePlus: GooglePlus, public linkedin: LinkedIn, public twitter:TwitterConnect) {
    console.log('Hello SocialNetworkProvider Provider');
  }


  facebookdata(){
  let promise = new Promise((resolve, reject) => {
    this.fb.login(['email','public_profile']).then( (response) => {
        console.log('Facebook response',response);
        this.fb.api("me?fields=id,name,picture.height(961),email,link",this.params).then( (res) => {
          console.log('res facebook api',res)
          resolve(res);
          this.fb.logout();
        }).catch((error) => {
          console.log('api error',error)
          reject(error);
        });
      }).catch((error) => {
        console.log(error)
        reject(error);
      });
    });
    return promise;
  }

  facebookdataex(){
  console.log('called')
    let promise = new Promise((resolve, reject) => {
      resolve('called resolve');
    });
    return promise;
  }

  googledata(){
    let promise = new Promise((resolve, reject) => {
      this.googlePlus.login({
        'webClientId': '637371144664-fr1fqeitvbm2sb45ani9g3ord41e52st.apps.googleusercontent.com',
        'offline': true
      }).then((res) => {
        console.log('googleresponse',res);
        resolve(res);
      }).catch((err) => {
        console.error('google error',err);
        reject(err);
      });
    });
    return promise;
  }

  LinkedIndata(){
     let scopes:any = ['r_basicprofile', 'r_emailaddress'];
    let promise = new Promise((resolve, reject) => {
      this.linkedin.login(scopes, true).then((res) => {
        this.linkedin.getRequest('people/~').then((res) => {
          console.log(res);
          resolve(res);
        }).catch((e) => {
          reject(e);
          console.log(e);
        });
      }).catch((e) => {
        reject(e);
        console.log('Error logging in', e);
      });
    });
    return promise;
  }

  Twitterdata(){
     let scopes:any = ['r_basicprofile', 'r_emailaddress'];
    let promise = new Promise((resolve, reject) => {
      this.twitter.login().then((onSuccess) => {
        resolve(onSuccess);
        console.log(onSuccess);
      }).catch((e) => {
        reject(e);
        console.log('Error logging in', e);
      });
    });
    return promise;
  }


}
